
protagonist.png
size: 416,156
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: true
  xy: 358, 52
  size: 40, 27
  orig: 42, 29
  offset: 1, 1
  index: -1
head-front
  rotate: true
  xy: 358, 94
  size: 60, 56
  orig: 64, 59
  offset: 2, 2
  index: -1
heart-frame
  rotate: false
  xy: 180, 2
  size: 176, 152
  orig: 208, 184
  offset: 16, 16
  index: -1
heart-full
  rotate: false
  xy: 2, 2
  size: 176, 152
  orig: 208, 184
  offset: 16, 16
  index: -1
