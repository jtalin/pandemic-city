﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TimeConverter
{
    public static string MakeTimeString(float t)
    {
        float ms = (Mathf.Floor(t * 100) % 100);
        int seconds = (int)(t % 60);
        t /= 60;
        int minutes = (int)(t % 60);
        t /= 60;
        int hours = (int)(t % 24);

        if (hours > 0)
            return string.Format("{0}:{1}:{2}:{3}", hours.ToString("00"), minutes.ToString("00"), seconds.ToString("00"), ms.ToString("00"));
        else
            return string.Format("{0}:{1}:{2}", minutes.ToString("00"), seconds.ToString("00"), ms.ToString("00"));
    }
}
