﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using Spine.Unity;

public class Pedestrian : MonoBehaviour
{
    private const float moveSpeed = 2f; // Speed at which pedestrians move.
    private const float infectionChance = 0.25f; // Chance this pedestrian will infect another pedestrian upon collision.
    [SerializeField] public Animator glowAuraAnimator;

    public bool isInfected = false; // Is this pedestrian infected?;
    private Node previousNode; // Node this pedestrian is coming from.
    private Node currentNode; // Node this pedestrian is traveling towards.
    private SkeletonAnimation animator;

    // Initialize is called upon object creation.
    public void Initialize(Node node)
    {
        // Set a random skin.
        animator = GetComponent<SkeletonAnimation>();
        var skins = animator.skeleton.Data.Skins.ToArray();
        var chosenSkin = skins[Random.Range(0, skins.Length)];
        animator.Skeleton.SetSkin(chosenSkin.Name);
        animator.Skeleton.SetSlotsToSetupPose();
        animator.AnimationState.Apply(animator.Skeleton);

        animator.AnimationState.SetAnimation(0, "walk", true);

        previousNode = node;
        currentNode = node;
    }

    // Update is called once per frame.
    private void Update()
    {
        if(currentNode != null)
        {
            // Moves the pedestrian towards the target node.
            Vector3 currentPos = transform.position; // Should be V2
            Vector3 targetPos = currentNode.transform.position; // Should be V2
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(currentPos, targetPos, step);
            // Checks if the pedestrian is close enough to the target node to receive a new destination.
            if (currentNode.LiesWithinBoundary(transform.position))
            {
                Node pNode = previousNode;
                previousNode = currentNode;
                currentNode = currentNode.GetNewNode(pNode);
            }
        }
    }

    public void Infect()
    {
        if (isInfected)
            return;
        isInfected = true;
    }

    public void Blimp(float delay)
    {
        glowAuraAnimator.SetTrigger("StartGlow");
    }

    // OnCollisionEnter2D is called on the frame when this object first enters the hitbox of another collider.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        var pedestrianObject = collision.gameObject.GetComponent<Pedestrian>();
        var playerObject = collision.gameObject.GetComponent<Player>();
        // On collision with a pedestrian, infects another pedestrian if conditions are met.
        if (pedestrianObject != null)
        {
            if (isInfected)
                if (Random.Range(0f, 1f) < infectionChance)
                    pedestrianObject.Infect();
        }
        // On collision with the player, infect the player.
        else if(playerObject != null)
        {
            if (isInfected)
                playerObject.GetHit();
        }
    }
}
