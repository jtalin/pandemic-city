﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class Car : MonoBehaviour
{
    private const float minimumCarSpeed = 9f; // Minimum speed the car will travel at.
    private const float maximumCarSpeed = 10f; // Maximum speed the car will travel at.
    private float accMinTime = 0f; // Minimum time it takes the car to accelerate up to its default speed.
    private float accMaxTime = 2f; // Maximum time it takes the car to accelerate up to its default speed.
    private float speedDamageTreshold = 1f; // Treshold at, when lower, a moving car does not damage the player.

    private Carspawner spawner;
    private float defaultSpeed;
    private float speed;
    public bool isResettingSpeed = false;

    public void Initialize(SpawnDirection direction, Carspawner s)
    {
        // Randomize car speed.
        defaultSpeed = Random.Range(minimumCarSpeed, maximumCarSpeed);
        spawner = s;

        // Set car's rotation.
        if (direction == SpawnDirection.South)
            transform.rotation = Quaternion.Euler(0, 0, 180f);
        else if (direction == SpawnDirection.North)
            transform.rotation = Quaternion.Euler(0, 0, 0f);
        else if (direction == SpawnDirection.East)
            transform.rotation = Quaternion.Euler(0, 0, 270f);
        else if (direction == SpawnDirection.West)
            transform.rotation = Quaternion.Euler(0, 0, 90f);

        StartCoroutine(ResetSpeed(true));
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    public void Stop()
    {
        speed = 0;
    }

    // Accelerates the car to its default speed. Pass true if this has to be done instantly.
    public IEnumerator ResetSpeed(bool ignoreAcceleration)
    {
        isResettingSpeed = true;
        if (!ignoreAcceleration)
        {
            // Calculates the difference between the current speed and the default speed.
            // Increases speed every frame for x seconds until default speed is reached.
            float startSpeed = speed;
            float speedIncrease = defaultSpeed - speed;
            float acc = Random.Range(accMinTime, accMaxTime);
            float timePassed = 0f;
            while(timePassed < acc)
            {
                timePassed += Time.deltaTime;
                float f = timePassed / accMaxTime;
                speed = startSpeed + (f * speedIncrease);
                yield return null;
            }
        }
        speed = defaultSpeed;
        isResettingSpeed = false;
    }

    // Called by CarDespawn.
    public void Destroy()
    {
        // Calls the spawner to signal a car has been destroyed.
        spawner.OnCarDespawn();
        Destroy(gameObject);
    }

    // Collision is called when two collision hitboxes hit. 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Does not damage the player if speed of the car is lower than 1.
        if (speed < speedDamageTreshold)
            return;
        var player = collision.gameObject.GetComponent<Player>();
        if (player != null)
        {
            player.GetHit();
            AudioManager.PlaySound("honk");
        }
    }
}
