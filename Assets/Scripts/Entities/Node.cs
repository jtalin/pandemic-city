﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    // Which directions will the script search for connecting neighbours?
    [SerializeField] public bool connectNorth; // Set in Prefab.
    [SerializeField] public bool connectSouth; // Set in Prefab.
    [SerializeField] public bool connectWest; // Set in Prefab.
    [SerializeField] public bool connectEast; // Set in Prefab.

    private const float xBoundary = 0.35f; // Node collision boundary size.
    private const float yBoundary = 0.35f; // Node collision boundary size.
    private const float stepIncrease = 0.25f; // Size of the next step in a direction to find Nodes.
    private const int numberOfSteps = 50; // How many steps will be taken to find a node in each direction.

    private List<Node> linkedNodes = new List<Node>(); // List of Neighbours.
    private List<Node> excludedNodes = new List<Node>(); // (Temporarily) Excluded Neighbours.

    // Searches and adds neighbouring nodes.
    public void FindNeighbours(List<Node> nodes)
    {
        if (connectNorth)
        {
            Node n = GetClosestNode(nodes, new Vector2(0, stepIncrease));
            if (n != null)
                linkedNodes.Add(n);
        }
        if (connectSouth)
        {
            Node n = GetClosestNode(nodes, new Vector2(0, -stepIncrease));
            if (n != null)
                linkedNodes.Add(n);
        }
        if (connectWest)
        {
            Node n = GetClosestNode(nodes, new Vector2(-stepIncrease, 0));
            if (n != null)
                linkedNodes.Add(n);
        }
        if (connectEast)
        {
            Node n = GetClosestNode(nodes, new Vector2(stepIncrease, 0));
            if (n != null)
                linkedNodes.Add(n);
        }

        if (gameObject.name == "Node (1,1)")
            foreach (Node n in linkedNodes)
                Debug.Log(n);
    }

    private Node GetClosestNode(List<Node> nodes, Vector2 direction)
    {
        Vector2 center = transform.position;
        Vector2 distance = new Vector2(0, 0);
        for(int i = 0; i < numberOfSteps; i++)
        {
            distance += direction;
            foreach (Node n in nodes)
            {
                if (n == this)
                    continue; // Ignore own node.
                if (n.LiesWithinBoundary(center + distance))
                    return n;
            }
        }
        return null;
    }

    // Returns a neighbouring node. Tries to exclude the previousNode variable if possible.
    public Node GetNewNode(Node previousNode)
    {
        List<Node> nodes = new List<Node>(linkedNodes);
        foreach (Node n in excludedNodes)
            nodes.Remove(n);

        // Return this node if there are no nodes assigned.
        if (nodes.Count == 0)
            return this;

        // Returns the only available node if there is only one neighbour.
        if (nodes.Count == 1)
            return linkedNodes[0];

        // Otherwise returns a random node.
        nodes.Remove(previousNode);
        int random = Random.Range(0, nodes.Count);
        return nodes[random];
    }

    // Returns true if pos lies within node collision bounds.
    public bool LiesWithinBoundary(Vector2 pos)
    {
        Vector2 center = transform.position;
        if (pos.x < center.x + xBoundary && pos.x > center.x - xBoundary)
            if (pos.y < center.y + yBoundary && pos.y > center.y - yBoundary)
                return true;
        return false;
    }

    // Used by traffic Lights to temporarily exclude remove nodes.
    public void ExcludeNode(Node node)
    {
        excludedNodes.Add(node);
    }

    public void IncludeNode(Node node)
    {
        excludedNodes.Remove(node);
    }

    // OnDrawGizmos is only executed within the Unity3D.
    // It draws lines for easier overview of and assignment of node neighbours.
    #if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, 0.25f);
        if (UnityEditor.Selection.Contains(gameObject))
            for (int i = 0; i < linkedNodes.Count; i++)
                Gizmos.DrawLine(transform.position, linkedNodes[i].transform.position);
        Gizmos.color = Color.red;

        if (connectSouth)
            Gizmos.DrawLine(transform.position, transform.position - Vector3.up);
        if (connectNorth)
            Gizmos.DrawLine(transform.position, transform.position - Vector3.down);
        if (connectEast)
            Gizmos.DrawLine(transform.position, transform.position - Vector3.left);
        if (connectWest)
            Gizmos.DrawLine(transform.position, transform.position - Vector3.right);

    }
    #endif
}