﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intersection : MonoBehaviour
{
    private const float minimumSwitchLightInterval = 6f; // Minimum time it takes for lights on this intersection to swap.
    private const float maximumSwitchLightInterval = 8f; // Maximum time it takes for lights on this intersection to swap.

    private TrafficLight[] lights;
    private float currentInterval;
    private float timePassed;

    private void Awake()
    {
        lights = GetComponentsInChildren<TrafficLight>();
        for (int i = 0; i < lights.Length; i++)
            StartCoroutine(lights[i].SwitchLight());
        SetNewInterval();
    }

    // Update is called once per frame
    void Update()
    {
        // NOTE: Light's initial status is set in inspector/prefab.
        timePassed += Time.deltaTime;
        if(timePassed >= currentInterval)
        {
            SetNewInterval();
            timePassed = 0f;
            for (int i = 0; i < lights.Length; i++)
                StartCoroutine(lights[i].SwitchLight());
        }
    }

    private void SetNewInterval()
    {
        currentInterval = Random.Range(minimumSwitchLightInterval, maximumSwitchLightInterval);
    }
}
