﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDespawner : MonoBehaviour
{
    // Is called when a collider hits a collider with isTrigger enabled.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Destroys the car object when it is touched by one.
        var car = collision.gameObject.GetComponent<Car>();
        if (car != null)
            car.Destroy();
    }
}
