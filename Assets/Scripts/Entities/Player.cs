﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using Spine.Unity;
using UnityEngine.Experimental.Rendering.Universal;

public class Player : MonoBehaviour
{
    private float moveSpeed = 5f; // Speed at which the PC moves.
    public const int maxBatteryCharges = 5; // Number of times the player can use the radar. Graphics are made for 5 charges.
    public const int maxLife = 3; // Number of Lives the player has.
    private const float lightExpandTimer = 1f; // How long it takes for the radar light to expand fully.
    private const float maxLightExpansion = 6f; // Furthest extend of radar light expansion.
    private const float invulnerableTimer = 0.5f; // How long is the player immune after being hit?
    private const float tapTreshold = 0.15f; // Time for the mouse has to be held down for the input to registrer as a tap.

    private float timeHeldDown = 0f; // Amount of time the mouse has been held down.
    private Camera cam;
    private Light2D radarLight;
    private bool invulnerable = false;

    public int batteryCharges;
    public int life;

    private void Awake()
    {
        batteryCharges = maxBatteryCharges;
        life = maxLife;
        cam = Camera.main;
        radarLight = GetComponentInChildren<Light2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Advertisement.isShowing)
            return;
        if (Input.GetMouseButton(0))
        {
            timeHeldDown += Time.deltaTime;
            // Dragging
            if (timeHeldDown >= tapTreshold)
            {
                Vector3 currentPos = transform.position;

                float x = cam.ScreenToWorldPoint(Input.mousePosition).x;
                float y = cam.ScreenToWorldPoint(Input.mousePosition).y;
                Vector3 targetPos = new Vector3(x, y, transform.position.z);

                float step = moveSpeed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(currentPos, targetPos, step);
            }
        }
        // Release after Drag
        else if(timeHeldDown > tapTreshold)
            timeHeldDown = 0f;
        // Tap
        else if(timeHeldDown > 0f)
        {
            UseRadar();
            timeHeldDown = 0f;
        }
    }

    private void UseRadar()
    {
        if (batteryCharges > 0)
        {
            batteryCharges--;
            StopAllCoroutines(); // This only stops Coroutines from this script!
            StartCoroutine(ExpandRadarLight());
            Game.singleton.UseRadar(lightExpandTimer / 4);
            Game.singleton.ui.SetBatteryCharge(maxBatteryCharges, batteryCharges);
        }
    }

    // Called from Pedestrian and Car hitbox collisions.
    public void GetHit()
    {
        if (invulnerable) // Ignore if within invulerable timer.
            return;
        GetComponent<SkeletonAnimation>().AnimationState.SetAnimation(0, "hurt", false);
        GetComponent<SkeletonAnimation>().AnimationState.AddAnimation(0, "walk", true, 0);
        life--;
        Game.singleton.ui.SetHearts(maxLife, life);
        if (life <= 0)
            StartCoroutine(Game.singleton.EndGame(false));
        else StartCoroutine(SetInvulnerable());
    }

    // Handles Radar Light Expansion
    private IEnumerator ExpandRadarLight()
    {
    	var startScale = new Vector3(0f, 0f, 1f);
    	var endScale = new Vector3(maxLightExpansion, maxLightExpansion, 1f);
        float timePassed = 0f;
        while(timePassed < lightExpandTimer)
        {
            timePassed += Time.deltaTime;
            float f = timePassed / lightExpandTimer;
            var scale = Vector3.Lerp(startScale, endScale, f);
            radarLight.transform.localScale = scale;
            yield return null;
        }
        radarLight.transform.localScale = startScale;
    }

    // Handles the invulnerable Timer.
    private IEnumerator SetInvulnerable()
    {
        invulnerable = true;
        float timePassed = 0f;
        while(timePassed < invulnerableTimer)
        {
            timePassed += Time.deltaTime;
            yield return null;
        }
        invulnerable = false;
    }
}
