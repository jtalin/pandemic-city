﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.Experimental.Rendering.Universal;

public class TrafficLight : MonoBehaviour
{
    [SerializeField] public List<Node> connectedNodes; // Set in Prefab
    [SerializeField] public Light2D stopLight; // Set in Prefab
    [SerializeField] public Color greenColor; // Set in Prefab
    [SerializeField] public Color yellowColor; // Set in Prefab
    [SerializeField] public Color redColor; // Set in Prefab
    [System.NonSerialized] private float yellowDuration = 3f; // Time in seconds light stays at yellow.

    public bool isGreenLight = false;
    List<Car> carsWaiting = new List<Car>();

    public IEnumerator SwitchLight()
    {
        if (isGreenLight)
        {
            isGreenLight = false;
            stopLight.color = yellowColor;
            yield return new WaitForSeconds(yellowDuration);
            stopLight.color = redColor;

            // A traffic light needs 2 connecting nodes.
            if (connectedNodes.Count == 2)
            {
                connectedNodes[0].IncludeNode(connectedNodes[1]);
                connectedNodes[1].IncludeNode(connectedNodes[0]);
            }
        }
        else
        {
            isGreenLight = true;
            stopLight.color = greenColor;
            foreach (Car c in carsWaiting)
                StartCoroutine(c.ResetSpeed(false));
            carsWaiting = new List<Car>();

            // A traffic light needs 2 connecting nodes.
            if (connectedNodes.Count == 2)
            {
                connectedNodes[0].ExcludeNode(connectedNodes[1]);
                connectedNodes[1].ExcludeNode(connectedNodes[0]);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var car = collision.gameObject.GetComponent<Car>();
        if (car != null)
            if (!isGreenLight)
            {
                car.Stop();
                carsWaiting.Add(car);
            }
    }
}