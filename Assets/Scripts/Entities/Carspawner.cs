﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public enum SpawnDirection { South, North, West, East }

public class Carspawner : MonoBehaviour
{
    private const float minWaitFactor = 4; // Minimum amount of time before a new car spawns.
    private const float maxWaitFactor = 12f; // Maximum amount of time before a new car spawns.
    [SerializeField] public SpawnDirection direction; // Direction that spawned cars travel in. Set in Inspector.
    private GameObject carPrefab;

    private void Awake()
    {
        carPrefab = Resources.Load("Prefabs/Car") as GameObject; // Loads the car prefab from the Resources Folder.
        StartCoroutine(SpawnCar());
    }

    // Triggered by Car.
    public void OnCarDespawn()
    {
        if(enabled)
            StartCoroutine(SpawnCar());
    }

    private IEnumerator SpawnCar()
    {
        // Randomize time to wait.
        float wait = Random.Range(minWaitFactor, maxWaitFactor);
        yield return new WaitForSeconds(wait);
    
        GameObject carObject = Instantiate(carPrefab, Game.singleton.transform);
        // Set the car's position to the position of the spawner.
        Vector3 pos = transform.position; 
        carObject.transform.position = pos;

        var car = carObject.GetComponent<Car>();
        car.Initialize(direction, this);
    }

    // OnDrawGizmos is only executed within the Unity3D.
    // It draws lines for easier overview of and assignment of node neighbours.
    #if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.25f);
        if (direction == SpawnDirection.South)
            Gizmos.DrawLine(transform.position, transform.position - Vector3.up);
        else if (direction == SpawnDirection.North)
            Gizmos.DrawLine(transform.position, transform.position - Vector3.down);
        else if (direction == SpawnDirection.East)
            Gizmos.DrawLine(transform.position, transform.position - Vector3.left);
        else if (direction == SpawnDirection.West)
            Gizmos.DrawLine(transform.position, transform.position - Vector3.right);
    }
    #endif
}
