﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Store : MonoBehaviour
{
    [SerializeField] public Color poppedColor;
    [SerializeField] public Light2D storeLight;
    [SerializeField] public BoxCollider2D c;
    public bool isObjective;

    // Called by game to enable a store to set this as an objective.
    public void EnableStore()
    {
        storeLight.enabled = true;
        c.enabled = false;
        isObjective = true;
        
    }

    // Called after an enabled store is entered.
    public void DisableStore()
    {
        isObjective = false;
        GetComponent<Animator>().SetTrigger("StoreVisited");
        AudioManager.PlaySound("visit_store");
    }

    // Triggers when hit by the main store collision trigger.
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (isObjective)
        {
            DisableStore();
            Game.singleton.VisitStore();
        }
    }
}
