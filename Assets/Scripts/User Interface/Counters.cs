﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Counters : MonoBehaviour
{
    public static Counters singleton;
    [SerializeField] public Game game;
    [SerializeField] public TextMeshProUGUI infectionCounter;
    [SerializeField] public TextMeshProUGUI timer;
    [SerializeField] public TextMeshProUGUI shopCounter;
    [SerializeField] public TextMeshProUGUI endStage;
    [SerializeField] public Image fadeImage;

    private const float fadeTimer = 1f;

    private void Awake()
    {
        singleton = this;
    }

    private void Update()
    {
        if (singleton == null)
            return;
        UpdateTimer();
        UpdateInfectionCounter();
        UpdateStoreCounter();
    }

    private void UpdateTimer()
    {
        float t = Game.GetTimePassed();
        if(timer != null)
            timer.text = TimeConverter.MakeTimeString(t);
    }

    private void UpdateInfectionCounter()
    {
        if (game == null)
            return;
        List<Pedestrian> pedestrians = game.pedestrians;
        int sickAmount = 0;
        foreach (Pedestrian p in pedestrians)
            if (p.isInfected)
                sickAmount++;
        float f = (float)sickAmount / pedestrians.Count;
        if (infectionCounter != null)
            infectionCounter.text = "Infected: " + Mathf.RoundToInt((f * 100)) + "%";
    }

    private void UpdateStoreCounter()
    {
        if (shopCounter == null)
            return;
        string sv = "Stores Visited: ";
        if (Game.singleton != null)
        { 
            sv += Game.singleton.numberOfStoresVisited + "/" + Game.numberOfStoresToVisit;
        }
        shopCounter.text = sv;
    }

    public static void DisplayStageCompleteMessage()
    { 
        if (singleton != null)
            singleton.SetEndScreen();
    }

    // Just a helper method.
    private void SetEndScreen()
    {
        float t = Game.GetTimePassed();
        string time = TimeConverter.MakeTimeString(t);
        endStage.text = "Stage Complete! \n " + time;
        StartCoroutine(FadeScreen());
    }

    public IEnumerator FadeScreen()
    {
        Color startColor = fadeImage.color;
        Color endColor = new Color(startColor.r, startColor.g, startColor.b, 1f);
        float timePassed = 0f;
        while (timePassed < fadeTimer)
        {
            timePassed += Time.deltaTime;
            float f = timePassed / fadeTimer;
            fadeImage.color = Color.Lerp(startColor, endColor, f);
            yield return null;
        }
    }
}
