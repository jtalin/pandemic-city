﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI[] scoreTexts;
    [SerializeField] public Button soundButton;
    [SerializeField] public Sprite offButtonSoundSprite;
    [SerializeField] public Sprite onButtonSoundSprite;

    [SerializeField] public Image tutorialMainScreen;
    [SerializeField] public Image[] tutorialSplashes;

    private bool isShowingTutorial = false;
    private int tutorialStep = 0;

    private void Awake()
    {
        Time.timeScale = 1f;
        Input.backButtonLeavesApp = true; // If the player presses the back button, exit application.
        SetHighScores();

        if (PlayerPrefs.GetInt("sound") == 0)
            SetSound(false);
        else SetSound(true);
    }

    private void Update()
    {
        if(isShowingTutorial)
            if(Input.GetMouseButtonDown(0))
            {
                tutorialStep++;
                if(tutorialStep >= 4)
                {
                    tutorialStep = 0;
                    isShowingTutorial = false;
                    tutorialMainScreen.transform.SetAsFirstSibling();
                    for (int i = 0; i < tutorialSplashes.Length; i++)
                        tutorialSplashes[i].enabled = false;
                }
                else
                    tutorialSplashes[tutorialStep].transform.SetAsLastSibling();
            }
    }

    private void SetHighScores()
    {
        Highscores hs;
        try { hs = JsonUtility.FromJson<Highscores>(PlayerPrefs.GetString("highscores"));}
        catch { hs = new Highscores(); }
        if (hs == null)
        {
            hs = new Highscores();
        }
        List<float> highscores = hs.highscores;
        if (highscores == null)
            highscores = new List<float>();
        foreach (TextMeshProUGUI t in scoreTexts)
            t.text = "";
        for (int i = 0; i < highscores.Count; i++)
            scoreTexts[i].text = TimeConverter.MakeTimeString(highscores[i]);
    }

    // Assigned to Button in Scene.
    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void ShowTutorial()
    {
        for (int i = 0; i < tutorialSplashes.Length; i++)
            tutorialSplashes[i].enabled = true;
        isShowingTutorial = true;
        tutorialMainScreen.transform.SetAsLastSibling();
        tutorialSplashes[0].transform.SetAsLastSibling();
    }

    public void ChangeSound()
    {
        if (PlayerPrefs.GetInt("sound") == 0)
            SetSound(true);
        else SetSound(false);
    }

    public void SetSound(bool on)
    {
        if (on)
        {
            PlayerPrefs.SetInt("sound", 1);
            soundButton.image.sprite = onButtonSoundSprite;
            AudioListener.volume = 1f;
        }
        else
        {
            PlayerPrefs.SetInt("sound", 0);
            soundButton.image.sprite = offButtonSoundSprite;
            AudioListener.volume = 0f;
        }
    }
}