﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour
{
    [SerializeField] public Image batteryCharges;
    [SerializeField] public Image heartCharges;

    public void SetBatteryCharge(int totalCharges, int charges)
    {
        float p = (float)charges / totalCharges;
        batteryCharges.fillAmount = p;
    }

    public void SetHearts(int totalHearts, int hearts)
    {
        float p = (float)hearts / totalHearts;
        heartCharges.fillAmount = p;
    }
}
