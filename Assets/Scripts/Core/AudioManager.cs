﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager singleton;
    public List<AudioSource> audioFiles = new List<AudioSource>();

    private const float defaultMusicVolume = 0.1f; // Default volume of the music.

    private void Awake()
    {
        if (AudioManager.singleton == null)
        {
            singleton = this;
            foreach (AudioSource source in GetComponents<AudioSource>())
                audioFiles.Add(source);

        }
        else Destroy(this);
    }

    public static void PlaySound(string name)
    {
        if (singleton != null)
            foreach (AudioSource a in singleton.audioFiles)
                if (a.clip.name == name)
                    a.Play();
    }

    public static void EnableSound()
    {
        if (singleton != null)
            foreach (AudioSource a in singleton.audioFiles)
                a.volume = defaultMusicVolume;
    }

    public static void DisableSound()
    {
        if(singleton != null)
            foreach (AudioSource a in singleton.audioFiles)
                a.volume = 0f;
    }
}
