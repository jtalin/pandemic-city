﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
using SharpJson;

public class Game : MonoBehaviour
{
    public const int numberOfStoresToVisit = 3; // Number of stores to visit for victory.
    public const int initialInfected = 5; // Amount of pedestrians that will start as 'infected'.
    public const int numberOfPedestriansToSpawn = 2; // Number of pedestrians that get spawned at every node.
    public const float mainMenuDelay = 2f; // Time it takes for the player to return to the main menu after ending the game.
    public static Game singleton;
    public UserInterface ui;
    private Player player;
    private List<Node> nodes = new List<Node>();
    public List<Pedestrian> pedestrians = new List<Pedestrian>();

    public float timeSinceLevelStart = 0f;
    public int numberOfStoresVisited;
    private bool hasGameEnded = false;

    private void Awake()
    {
        // Input.backButtonLeavesApp is set to false for in-game. Instead, the back button returns you to the main menu.
        Input.backButtonLeavesApp = false; 
        singleton = this;
        InitializeNodes();
        SpawnStores();
        SpawnPedestrians();
        SpawnPlayer();
    }

    private void Start()
    {
        AudioManager.PlaySound("rain");
    }

    // Update runs every frame.
    private void Update()
    {
        // Keycode.Escape translates to "back" button on android.
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("Main");

        // Pause game while showing advertisement.
        if (!Advertisement.isShowing && !hasGameEnded)
            timeSinceLevelStart += Time.deltaTime;
    }

    // Set Nodes. Nodes are used by pedestrians for pathfinding.
    private void InitializeNodes()
    {
        foreach (Node node in GetComponentsInChildren<Node>())
            nodes.Add(node);
        foreach (Node node in nodes)
            node.FindNeighbours(nodes);
    }

    // Finds all Store components and activates a set number of them at random.
    private void SpawnStores()
    {
        var stores = new List<Store>();
        foreach (Store store in GetComponentsInChildren<Store>())
            stores.Add(store);
        for (int i = 0; i < numberOfStoresToVisit; i++)
        {
            var targetStore = stores[Random.Range(0, stores.Count)];
            targetStore.EnableStore();
            stores.Remove(targetStore);
        }
    }

    // Spawns pedestrians on nodes.
    private void SpawnPedestrians()
    {
        // Spawn Pedestrians
        GameObject pedestrian = Resources.Load("Prefabs/Pedestrian") as GameObject;
        for (int i = 0; i < nodes.Count; i++)
        {
            for (int j = 0; j < numberOfPedestriansToSpawn; j++)
            {
                GameObject go = Instantiate(pedestrian, transform);
                Vector3 pos = nodes[i].transform.position; // Should be V2
                go.transform.position = pos;
                go.GetComponent<Rigidbody2D>().mass = Random.Range(1, 10) * 10;
                Pedestrian p = go.GetComponent<Pedestrian>();
                p.Initialize(nodes[i].GetNewNode(null));
                pedestrians.Add(p);
            }
        }

        // Set Initial Infected
        List<Pedestrian> pCopy = new List<Pedestrian>(pedestrians);
        for(int i = 0; i < initialInfected; i++)
        {
            var infected = pCopy[Random.Range(0, pCopy.Count)];
            pCopy.Remove(infected);
            infected.Infect();
        }
    }

    // Finds the Player Component and randomly spawns it at one of the spawn locations.
    private void SpawnPlayer()
    {
        player = GetComponentInChildren<Player>();
        PlayerSpawner[] spawner = GetComponentsInChildren<PlayerSpawner>();
        var spawn = spawner[Random.Range(0, spawner.Length)];
        player.gameObject.transform.position = spawn.transform.position;
    }

    public IEnumerator EndGame(bool win)
    {
        // This prevents this coroutine from being called twice.
        if (hasGameEnded)
            yield break;
        hasGameEnded = true;

        SpriteRenderer[] srs = GetComponentsInChildren<SpriteRenderer>();
        float fadeTime = 1f;

        if (win)
        {
            Counters.DisplayStageCompleteMessage();
            yield return new WaitForSeconds(mainMenuDelay);
        }
        else
        {
            Time.timeScale = 0.5f;
            ui.gameObject.SetActive(false);
            foreach (Car car in GetComponentsInChildren<Car>())
                car.Stop();
            foreach (Carspawner spawner in GetComponentsInChildren<Carspawner>())
            {
                spawner.StopAllCoroutines();
                spawner.gameObject.SetActive(false);
            }
            Color startColor = new Color(1f, 1f, 1f, 1f);
            Color endColor = new Color(0f, 0f, 0f, 1f);
            float timePassed = 0f;
            while(timePassed < fadeTime)
            {
                timePassed += Time.deltaTime;
                float f = timePassed / fadeTime;
                Color c = Color.Lerp(startColor, endColor, f);
                for (int i = 0; i < srs.Length; i++)
                    srs[i].color = c;
                yield return null;
            }
            yield return new WaitForSeconds(mainMenuDelay - fadeTime);
        }
        ReturnToMainMenu(win);
    }

    public void ReturnToMainMenu(bool win)
    {
        // When win is passed, the game will take the timeSinceLevelStart variable and add it to the highscores list.
        if (win)
        {
            Highscores hs;
            //Deserialize playerpref string with JSON
            try { hs = JsonUtility.FromJson<Highscores>(PlayerPrefs.GetString("highscores")); }
            catch { hs = new Highscores(); }
            if (hs == null)
                hs = new Highscores();  

            // Get List, if list is null, create new list.
            List<float> highscores = hs.highscores;
            if (highscores == null)
                highscores = new List<float>();
            float thisScore = GetTimePassed();
            
            // Add score to list, sort list, cull to 10 if there are more than 10 entries.
            highscores.Add(thisScore);
            highscores.Sort();
            while (highscores.Count > 10)
                highscores.Remove(highscores[10]);
            
            //Serialize with JSON, save string to playerprefs.
            string jsonstring = JsonUtility.ToJson(hs); // <-- Jsons
            PlayerPrefs.SetString("highscores", jsonstring);
        }
        SceneManager.LoadScene("Main");
    }

    // Call this to show ad. When called within the game code, the game will be paused for the duration.
    public void ShowAd()
    {
        // Ads are not shown in editor. 
        #if UNITY_ANDROID 
        if (Advertisement.IsReady())
            Advertisement.Show();
        #endif
    }

    public void VisitStore()
    {
        numberOfStoresVisited++;

        if (numberOfStoresVisited >= numberOfStoresToVisit)
            StartCoroutine(EndGame(true)); 
        else ShowAd();
    }

    public void UseRadar(float delay)
    {
        for (int i = 0; i < pedestrians.Count; i++)
            if (pedestrians[i].isInfected)
                pedestrians[i].Blimp(delay);
    }

    // Static method used by UI to retrieve timepassed.
    public static float GetTimePassed()
    {
        if(singleton == null)
            return 0f;
        return singleton.timeSinceLevelStart;
    }
}

[System.Serializable]
public class Highscores
{
    public List<float> highscores = new List<float>();
}