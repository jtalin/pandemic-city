# pandemic-city
A top-down arcade game where the protagonist makes a grocery run while maintaining social distancing and avoiding infection and traffic.

# Requirements
- Unity 2020.1.0f1 installation
- Spine Runtime Unity package 
https://esotericsoftware.com/files/runtimes/unity/spine-unity-3.8-2020-08-06.unitypackage
- Spine Universal RP Shaders Unity package - Unity may have trouble automatically installing this one, if that happens download and add it from disk via the Package Manager.
https://esotericsoftware.com/files/runtimes/unity/com.esotericsoftware.spine.urp-shaders-3.8-Unity2019.3-2020-08-07.zip

# Modifying the game in Unity Editor

The Game scene is composed from Unity prefabs in a modular approach, allowing for quick and easy expansion without delving into the code. 

The game map is made out of block prefabs stored in Resources/Prefabs. These prefabs can be edited in a number of ways, including changing background image assets (and colliders to match), editing lights, store locations, and so on. New block prefabs can also be created - the easiest way to do this is by duplicating a current block and modifying the copy. Note that any new images added to the project must be set to 102.4 Pixels per Unit in the texture inspector to retain the same image size and resolution. 

To add a block to the map, simply drag it into the Game scene and place it where you want. The size of blocks (in units) should always be 1x1, allowing for easy snapping using the prefab's Transform component.

Apart from graphics, there are multiple other elements that can be changed in editor to control the game's behavior within the Game scene. Game scene can be found in Assets/Scenes. 

- **Car Spawners** are the bright yellow elements placed along the edges of the Game scene view. Cars will periodically spawn at these points and proceed to drive in the direction indicated by the element. The direction can be changed in the Carspawner component attached to the Spawner game object. Generally, Car Spawners should be placed at the beginnings
of street lanes.

- **Passenger Nodes** are bright red elements on the map that the passengers will pathfind between. Upon reaching a Node, every passenger will randomly choose one of the enabled cardinal directions and walk in that direction. Allowed directions can be set in the object's Node component. Generally, Passenger Nodes should be placed along sidewalks, but they can work anywhere - bearing in mind that the passengers can only move in straight lines in cardinal directions (up, down, left, right), a Node should be placed at any point you want a passenger to have a chance of changing the direction in which they walk.

- **Store Locations** repesent grocery stores which are objectives of the game. Placing a store prefab anywhere within a Block will automatically add it to the list of possible store locations that the game will randomly pick 3 from at the start of every run. Note that the block collider - Polygon Collider 2D component attached to the block game object inside the prefab - must be manually edited to "open up" a new store location, otherwise it will be impossible for the player to actually walk inside.

- **Traffic Lights** will stop cars on red anywhere it's placed.

In addition to scene and prefabs, animation objects for the game (particularly passengers, cars and stores) can be found in Assets/Animations and modified directly in the editor. 

# Modifying the game code

For more complex interventions and additins to the game must be implemented in the code. All the existing scripts are located in the Scripts folder.
- Assets/Scripts/Core/Game.cs contains the core game loop and logic. 
- Assets/Scripts/Entities contains scripted behavior for all entities currently in the game. 
- Assets/Scripts/User Interface contains code for both the in-game interface and the main menu screen.

For your convenience, you can also download the project source directly from Google Drive via the following link: https://drive.google.com/file/d/13iJa5Us_p4E_c_rdynTZU_Lbg2jehn-O/view?usp=sharing 